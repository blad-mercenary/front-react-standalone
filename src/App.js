import ListCharacter from './components/ListCharacter'
import Fight from './components/Fight'
import { heroes, enemies, fight } from './tools/datas'

import './App.scss'

function App() {
  return (
    <div className="game-board">
      <ListCharacter characters={heroes()} />
      <Fight fight={fight} />
      <ListCharacter characters={enemies()} />
    </div>
  )
}

export default App
