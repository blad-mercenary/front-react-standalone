import "../styles/actionButton.scss"

function ActionButton({ name }) {
   return (
      <button>
         {name}
      </button>
   )
}

export default ActionButton
