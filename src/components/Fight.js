import PlayerCardDual from "./PlayerCardDual"
import ActionButton from "./ActionButton"
import versus from '../images/versus.png'
import '../styles/fight.scss'
import { getImage } from "../tools/images"
import { getPossibleSelections, Team } from "@blad-mercenary/core"

function Fight({fight}) {
   const nextActiveFighters = fight.getNextActiveFighters();
   const activePlayer = nextActiveFighters.find(fighter => fighter.team === Team.player);
   const activeEnemy = nextActiveFighters.find(fighter => fighter.team === Team.enemy);

   const selections = getPossibleSelections(activePlayer, fight);
   const actions = Array.from(new Set(selections.map((selection) => selection.action.name)));

   let selectedAction = null;

   return (
      <div className="fight">
         <h1>BLaD</h1>
         <h3>Next:</h3>
         <div className="next-fights">
            {
               nextActiveFighters.map((fighter) => (
                  <div className="image-container">
                     <img src={getImage(fighter, 'portrait')} alt="Player" />
                  </div>
               ))
            }
         </div>
         <div className="dual">
            <PlayerCardDual character={activePlayer} />
            <div className="versus-image">
               <img src={versus} alt="Versus" />
            </div>
            <PlayerCardDual character={activeEnemy} />
         </div>
         <div className="action-buttons">
            <div className="actions">
               {
                  actions.map((action) => (<ActionButton name={action} selected={selectedAction === action} />))
               }
            </div>
            <div className="confirm">
               <button>
                  Validate
               </button>
            </div>
         </div>
      </div>
   )
}

export default Fight
