import PlayerCard from './PlayerCard'

import '../styles/characterList.scss'

function ListCharacter({ characters }) {
   return (
      <div className="character-list">
         {
            characters.map((character, index) => <PlayerCard key={index} {...character} />)
         }
      </div>
   )
}

export default ListCharacter
