import '../styles/playerCard.scss'

function PlayerCard(character) {
   return (
      <div className="player-card">
         <div className="image-container">
            <img src={character.image} alt="Player" />
         </div>
         <h3 className="player-name">{character.name}</h3>
         <div className="progress-bar-out">
            <div className="progress-bar-in" style={{ width: `${character.health}%` }}></div>
         </div>
         <div className="progress-bar-out">
            <div className="progress-bar-in" style={{ width: `${character.mana}%` }}></div>
         </div>
      </div>
   )
}

export default PlayerCard
