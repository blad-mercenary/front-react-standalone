import '../styles/playerCardDual.scss'
import { getImage } from '../tools/images'

function PlayerCardDual({ character }) {
   return (
      <div className="player-card-dual">
         <div className="details">
            <h3 className="player-name-dual"> {character.name} </h3>
            <div className="progress-bar-out">
               <div className="progress-bar-in" style={{ width: `${character.health / character.maxHealth * 100}%` }}></div>
            </div>
            <div className="progress-bar-out">
               <div className="progress-bar-in" style={{ width: `${character.mana / character.maxMana * 100}%` }}></div>
            </div>
         </div>
         <div className="image-container">
            <img src={getImage(character, 'sprite')} alt="Player" />
         </div>
      </div>
   )
}

export default PlayerCardDual
