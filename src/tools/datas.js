import { Fight, Characters, Team } from '@blad-mercenary/core';
import { getImage } from './images';

const fighters = [
	Characters.warrior(Team.player),
	Characters.warrior(Team.player),
	Characters.mage(Team.player),
	Characters.healer(Team.player),
	Characters.warrior(Team.enemy),
	Characters.warrior(Team.enemy),
	Characters.mage(Team.enemy),
	Characters.healer(Team.enemy)
]

export const fight = new Fight(fighters);

function getFighterData(team) {
   return fight.fighters
      .filter((fighter) => fighter.team === team)
      .map((fighter) => {
         return {
            name: fighter.name,
            health: fighter.health / fighter.maxHealth * 100,
            mana: fighter.mana / fighter.maxMana * 100,
            image: getImage(fighter, 'portrait')
         };
      });
}

export const heroes = () => getFighterData(Team.player);
export const enemies = () => getFighterData(Team.enemy);