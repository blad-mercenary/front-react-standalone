import warriorSprite from '../images/warrior.png'
import warriorPortrait from '../images/warrior-portrait.png'
import mageSprite from '../images/mage.png'
import magePortrait from '../images/mage-portrait.png'
import healerSprite from '../images/healer.png'
import healerPortrait from '../images/healer-portrait.png'

import warriorSpriteEnemy from '../images/warrior-enemy.png'
import warriorPortraitEnemy from '../images/warrior-portrait-enemy.png'
import mageSpriteEnemy from '../images/mage-enemy.png'
import magePortraitEnemy from '../images/mage-portrait-enemy.png'
import healerSpriteEnemy from '../images/healer-enemy.png'
import healerPortraitEnemy from '../images/healer-portrait-enemy.png'

const image = {
	warrior: {
		player: {
			sprite: warriorSprite,
			portrait: warriorPortrait
		},
		enemy: {
			sprite: warriorSpriteEnemy,
			portrait: warriorPortraitEnemy
		}
	},
	mage: {
		player: {
			sprite: mageSprite,
			portrait: magePortrait
		},
		enemy: {
			sprite: mageSpriteEnemy,
			portrait: magePortraitEnemy
		}
	},
	healer: {
		player: {
			sprite: healerSprite,
			portrait: healerPortrait
		},
		enemy: {
			sprite: healerSpriteEnemy,
			portrait: healerPortraitEnemy
		}
	}
}

export function getImage(character, type) {
	return image[character.type][character.team][type];
 }
 